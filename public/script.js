let appId = '6f287b6d54bee8794a41d7776558222c'
let units = 'metric'
let searchMethod
let request
const api = `http://api.openweathermap.org/data/2.5/weather?`

// ${searchMethod}=${searchTerm}&APPID=${appId}&units=${units} 
function getSearchMethod(searchTerm) {
    if(searchTerm.length === 5 && Number.parseInt(searchTerm) + '' == searchTerm) {
      return 'zip';
    }
    
    return 'q'
}

window.addEventListener('load', ()=> {
    request = api + 'q=Bitola' + '&APPID=' + appId + '&units=' + units 
    fetch(request)
    .then(result => {
        return result.json();
    }).then(result => {
        init(result);
    })

    setVisibility();
})

function searchWeather(searchTerm) {
    searchMethod = getSearchMethod(searchTerm)
    request = api + searchMethod + '=' + searchTerm + '&APPID=' + appId + '&units=' + units 
    fetch(request)
    .then(result => {
        return result.json();
    }).then(result => {
        init(result);
    })
} 

function init(resultFromServer) {
    switch (resultFromServer.weather[0].main) {
        case 'Clear':
            document.body.style.backgroundImage = 'url(../public/images/clear.jpg)';
            break

        case 'Clouds':
            document.body.style.backgroundImage = 'url(../public/images/clouds.jpg)';
            break

        case 'Rain':
        case 'Drizzle':
        case 'Mist':
            document.body.style.backgroundImage = 'url(../public/images/rain.jpg)';
            break;

        case 'Thunderstorm':
            document.body.style.backgroundImage = 'url(../public/images/storm.jpg)';
            break;
            
        case 'Snow':
            document.body.style.backgroundImage = 'url(../public/images/snow.jpg)';
            break;

        default:
            break;
        
    }

    let weatherDescriptionHeader = document.getElementById('weatherDescriptionHeader');
    let temperatureElement = document.getElementById('temperature');
    let humidityElement = document.getElementById('humidity');
    let windSpeedElement = document.getElementById('windSpeed');
    let cityHeader = document.getElementById('cityHeader');
    let weatherIcon = document.getElementById('documentIconImg');

    weatherIcon.src = 'http://openweathermap.org/img/wn/' + resultFromServer.weather[0].icon + '.png';

    let resultDescription = resultFromServer.weather[0].description;
    weatherDescriptionHeader.innerText = resultDescription.charAt(0).toUpperCase() + resultDescription.slice(1);

    temperatureElement.innerHTML = Math.floor(resultFromServer.main.temp) + '&#176';
    windSpeedElement.innerHTML = 'Winds at'  + Math.floor(resultFromServer.wind.speed) + 'm/s';
    cityHeader.innerHTML = resultFromServer.name;
    humidityElement.innerHTML = 'Humidity levels at ' + resultFromServer.main.humidity + '%';
    setVisibility();
}

function activatePlacesSearch() {
    let input = document.getElementById('searchInput');
    let autocomplete = new google.maps.places.Autocomplete(input);
}

function setVisibility(){
    let weatherContainer = document.getElementById('weatherContainer');
    weatherContainer.style.visibility = 'visible';
}

document.getElementById('searchBtn').addEventListener('click', () => {
    let searchTerm = document.getElementById('searchInput').value;

    // try {
    //     resultFromServer.name
    // } catch (error) {
    //     alert('ne e tocno')
    // }

    if(!searchTerm) {
        alert('Please try again later!')
    }
    
    searchWeather(searchTerm)
})